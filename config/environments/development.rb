Booday10::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true
  
  # For Paperclip
  Paperclip.options[:command_path] = "/usr/bin/"

  # config.paperclip_defaults = {
  #   storage: :s3,
  #   s3_credentials: {
  #     bucket: 'booday-s3',
  #     access_key_id: 'AKIAIZZKJY57RNEHHYRA',
  #     secret_access_key: 'O+ooIIZ3xCs9mlrTD6KF5uRbt3m/eV5fky/m26',
  #     s3_region: 'us-east-1',
  #   }
  # }
end