class CreateUsers < ActiveRecord::Migration

  def up
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :remember_digest

      t.timestamps
    end
    
    add_index :users, :name, unique: true
    add_index :users, :email, unique: true    
  end

  def down
  	drop_table	:users
  end
end