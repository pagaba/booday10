class CreatePins < ActiveRecord::Migration
	
  def up
    create_table :pins do |t|
      t.string :title
      t.text :content
      t.integer	:user_id

      t.timestamps
    end
    
    # add_index :pins, :user_id
    # add_index :pins, :title
    # add_index :pins, :content
  end

  def down
  	drop_table :pins  	
  end
end