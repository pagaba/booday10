class AddCategoryToPins < ActiveRecord::Migration

  def change
    add_reference :pins, :category, index: true, foreign_key: true
  end
end

# rails g migration AddCategoryToPins category:references