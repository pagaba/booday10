class Pin < ActiveRecord::Base
  
  self.per_page = 16
  # acts_as_votable
  belongs_to :user
  belongs_to :category
  validates :title, presence: true
  validates :category_id, presence: true
  validates :content, presence: true, length: { maximum: 3000 }
  
  # has_attached_file :image, styles: { large: "700x700>", medium: "300x300>", small: "100x100#", thumb: "300x180#" }
  has_attached_file :image, styles: { large: "700x700>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  # validates :image, attachment_presence: true

  def self.search(search)
    where("title LIKE ? OR content LIKE ?", "%#{search}%", "%#{search}%") 
  end

  # Prettifies the URLs
  def to_param
    "#{id} #{title[0,10]}".parameterize
  end
end