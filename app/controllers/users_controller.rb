class UsersController < ApplicationController

	before_action :logged_in_user, only: [:edit, :update]
	before_action :correct_user, only: [:edit, :update]
	before_action :set_user, only: [:show, :edit, :update]


	def show
		@pins = @user.pins.order(created_at: :desc).paginate(:page => params[:page])
	end

	def new
		redirect_to current_user if logged_in?
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			log_in @user
			redirect_to @user
		else
			render :new
		end
	end

	def edit		
	end

	def update
		@user = User.find(params[:id])
		if @user.update_attributes(user_params)
			redirect_to @user
		else
			render :edit
		end
	end

	private
	
		def user_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation)
		end

		# Before filters

		# Confirms the correct user.
		def correct_user
			@user = User.find(params[:id])
			redirect_to(root_url) unless current_user?(@user)
		end

		def set_user
			@user = User.find_by(id: params[:id])
		end
end