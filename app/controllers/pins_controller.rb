class PinsController < ApplicationController

  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_pin, only: [:show, :edit, :update, :destroy]
  before_action :correct_pin, only: [:edit, :update, :destroy]

  def index
    current_page = params[:page] || 1
    per_page = params[:per_page] || 16
    @pins = Pin.order('created_at DESC')

    if params[:search] && !params[:search].blank?
      @pins = Pin.search(params[:search])
      users = User.search(params[:search])
      unless users.blank?
        users.each do |user|
          @pins += user.pins
        end
      end
    end

    length = @pins.length

    @pins = WillPaginate::Collection.create(current_page, per_page, length) do |pager|
      pager.replace @pins[pager.offset, pager.per_page].to_a
    end
  end

  def soccer
    current_page = params[:page] || 1
    per_page = params[:per_page] || 16
    @pins = Pin.where(category_id: 2).order('created_at DESC')
    length = @pins.length

    @pins = WillPaginate::Collection.create(current_page, per_page, length) do |pager|
      pager.replace @pins[pager.offset, pager.per_page].to_a
    end
  end

  def basket_ball
    current_page = params[:page] || 1
    per_page = params[:per_page] || 16
    @pins = Pin.where(category_id: 1).order('created_at DESC')
    length = @pins.length

    @pins = WillPaginate::Collection.create(current_page, per_page, length) do |pager|
      pager.replace @pins[pager.offset, pager.per_page].to_a
    end
  end

  def show
    ids = Pin.pluck(:id).shuffle[0..7]
		@pins = Pin.where(id: ids)
  end

  def new
    @pin = current_user.pins.build
  end

  def edit
  end

  def create
    @pin = current_user.pins.build(pin_params)

    
      if @pin.save
        redirect_to @pin
      else
        render :new
      end
    
  end

  def update
    respond_to do |format|
      if @pin.update(pin_params)
        format.html { redirect_to @pin }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @pin.errors, status: :unprocessable_entity }
      end
    end
  end

  # def like
  #   @pin.liked_by current_user
  #   respond_to do |format|
  #     format.html {redirect_to :back}
  #     format.js
  #   end
  # end

  # def dislike
  #   @pin.disliked_by current_user
  #   respond_to do |format|
  #     format.html {redirect_to :back}
  #     format.js
  #   end
  # end

  def destroy
    @pin.destroy
    respond_to do |format|
      format.html { redirect_to pins_url }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_pin
      @pin = Pin.find(params[:id])
    end
    
    def correct_pin
      @pin = current_user.pins.find_by(id: params[:id])      
      redirect_to root_url if @pin.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pin_params
      params.require(:pin).permit(:content, :image, :title, :user_id, :category_id)
    end
end